#!/bin/sh

mkdir -p $HOME/.docker/cli-plugins
wget -qO $HOME/.docker/cli-plugins/docker-buildx \
	https://github.com/docker/buildx/releases/download/v0.10.4/buildx-v0.10.4.linux-amd64
chmod a+x $HOME/.docker/cli-plugins/docker-buildx
docker buildx create --name pantavisor-builder --use --bootstrap
