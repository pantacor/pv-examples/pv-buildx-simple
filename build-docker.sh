#!/bin/sh

tagname=$1
arch=$2

usage() {
	echo "$0 <tagname> <arch>"
        echo "    example: $0 mydocker arm64"
}

if [ -z "$tagname" ] || [ -z "$arch" ]; then
	usage
	exit 1
fi

docker buildx build --platform linux/$arch --output=type=docker --tag $tagname:$arch .

